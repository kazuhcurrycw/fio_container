#!/bin/bash

SC=$1

PVC_NAME=fio-${SC}
POD_NAME=fio-${SC}

cat << EOF > pod_${POD_NAME}.yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: $PVC_NAME
spec:
  accessModes:
  - ReadWriteOnce
  storageClassName: $SC
  resources:
    requests:
      storage: 64Gi
---
apiVersion: v1
kind: Pod
metadata:
  name: $POD_NAME
  labels:
    app: fio
spec:
  containers:
  - name: fio
    image: registry.gitlab.com/kazuhcurrycw/fio_container:v0.8
    imagePullPolicy: IfNotPresent
    volumeMounts:
    - mountPath: /mnt
      name: pvc-disk
  restartPolicy: Never
  volumes:
  - name: pvc-disk
    persistentVolumeClaim:
      claimName: $PVC_NAME
EOF

