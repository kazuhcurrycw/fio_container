#!/bin/bash

DIR=/tmp
DO_SEQ_R=1
DO_SEQ_W=1
DO_RANDOM=1
RW_IODEPTH="1 4 16 64"

if [ $# -eq 0 ]; then
	echo "use default parameters."
elif [ $# -eq 1 ]; then
	DIR=$1
elif [ $# -eq 3 ]; then
	DIR=$1
	DO_SEQ_R=$2
	DO_SEQ_W=$3
	DO_RANDOM=0
elif [ $# -ge 4 ] ;then
	DIR=$1
	DO_SEQ_R=$2
	DO_SEQ_W=$3
	shift;shift;shift
	RW_IODEPTH=$@
else
	echo "#Error! Argument wrong."
	echo "usage:"
	echo "  $0"
	echo "  $0 dir"
	echo "  $0 dir SeqRead SeqWrite"
	echo "  $0 dir SeqRead SeqWrite ReadWriteDepth ..."
fi

echo "Dir: $DIR"
echo "Seq Read: $DO_SEQ_R"
echo "Seq Write: $DO_SEQ_W"
echo "Random RW: $DO_RANDOM"
echo "IODEPTH:  $RW_IODEPTH"

if [ ! -d $DIR ]; then
	echo "$DIR not a directroy."
	exit 1
fi

# Configuration
FILEPREFIX=$DIR/fio_test
SIZE=16g
RUNTIME=60
SLEEPTIME=10

# Benchmark
#LOGFILE=fio_`date +'%Y%m%d-%H%M%S'`.log
LOGFILE=fio.log
NUMFILES=`echo $SIZE | tr -d g`
FILENAME=$FILEPREFIX.001

for FILESUFFIX in `seq 2 $NUMFILES` ; do
	FILENAME=$FILENAME:$FILEPREFIX.`printf "%03d" $FILESUFFIX`
done

# Sequential Read
if  [ $DO_SEQ_R == 1 ] ; then
	date "+%F %T : Start Seq Read"
	fio \
          -name=fio_seq_read \
          -filename=$FILENAME \
          -direct=1 \
          -ioengine=libaio \
          -runtime=$RUNTIME \
          -size=$SIZE \
          -readwrite=read \
          -blocksize=1m \
          -iodepth=256 >> $LOGFILE
	date "+%F %T : End Seq Read"
	sleep $SLEEPTIME
fi

# Sequential Write
if  [ $DO_SEQ_W == 1 ] ; then
	date "+%F %T : Start Seq Write"
	fio \
          -name=fio_seq_write \
          -filename=$FILENAME \
          -direct=1 \
          -ioengine=libaio \
          -runtime=$RUNTIME \
          -size=$SIZE \
          -readwrite=write \
          -blocksize=1m \
          -iodepth=256 >> $LOGFILE
	date "+%F %T : End Seq Write"
	sleep $SLEEPTIME
fi

# Random Read/Write
if [ $DO_RANDOM == 1 ] ; then
	for BLOCKSIZE in 8k; do
		for RATIO in 100 50 0; do
			for IODEPTH in $RW_IODEPTH; do
				date "+%F %T : Start Random R/W : bs=$BLOCKSIZE, ratio=$RATIO, qdepth=$IODEPTH"
				fio \
				  -name=fio_b${BLOCKSIZE}_r${RATIO}_q${IODEPTH} \
				  -filename=$FILENAME \
				  -direct=1 \
				  -ioengine=libaio \
				  -runtime=$RUNTIME \
				  -size=$SIZE \
				  -readwrite=randrw \
				  -blocksize=$BLOCKSIZE \
				  -iodepth=$IODEPTH \
				  -rwmixread=$RATIO \
				  -numjobs=4 \
				  -group_reporting >> $LOGFILE
				date "+%F %T : End Random R/W : bs=$BLOCKSIZE, ratio=$RATIO, qdepth=$IODEPTH"
				sleep $SLEEPTIME
			done
		done
	done
fi

# Finalize
for FILESUFFIX in `seq 1 $NUMFILES`; do
	rm -f $FILEPREFIX.`printf "%03d" $FILESUFFIX`
done

