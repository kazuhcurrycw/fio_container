#!/bin/bash

if [ $# == 0 ]; then
	DIR=/mnt/
else
	DIR=$1
fi

cd $DIR

fio.sh $@

cat fio.log

echo 
echo "# Summary"
cat fio.log | fio_summarize.pl

