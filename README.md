# fio_container

## コンテナで起動

測定したいデバイス上のディレクトリをボリュームとして/mntにアタッチし起動する。

```
# docker run --name fio -v /home/myvol:/mnt registry.gitlab.com/kazuhcurrycw/fio_container:v0.8
```

コンテナの実行コマンドの引数で実行対象試験を選べる。


```
kick_fio.sh /mnt SeQRをやるか? SeqWをやるか? RandomRWのIODEPTH 
```

例：Sequential Readのみ試験

```
kick_fio.sh /mnt 1 0
```

例：Sequential Writeのみ試験

```
kick_fio.sh /mnt 0 1
```

Random RWのIODEPTH 128のみ

```
kick_fio.sh /mnt 0 0 128
```

## Podで起動

測定したいデバイスをボリューム（PV）としてPodにアタッチし起動する。

付属のgen_pod.shでPodとPVCのマニフェスト例を生成できる。引数にはStorageClass名を与える。

```
# ./gen_pod.sh default

# cat pod_fio-default.yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: fio-default
spec:
  accessModes:
  - ReadWriteOnce
  storageClassName: default
  resources:
    requests:
      storage: 64Gi
---
apiVersion: v1
kind: Pod
metadata:
  name: fio-default
  labels:
    app: fio
spec:
  containers:
  - name: fio
    image: registry.gitlab.com/kazuhcurrycw/fio_container:v0.8
    imagePullPolicy: IfNotPresent
    volumeMounts:
    - mountPath: /mnt
      name: pvc-disk
  restartPolicy: Never
  volumes:
  - name: pvc-disk
    persistentVolumeClaim:
      claimName: fio-default
```

