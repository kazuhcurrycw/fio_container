#!/usr/bin/perl

use strict;
use warnings;

my (%sequential, %sequential_iops,  %random, %random_bw);
my ($type, $blocksize, $ratio, $iodepth, $header);

while (my $line = <STDIN>) {
	if ($line =~ /^fio_seq_(\w+)/) {
		$type = $1;
	} elsif ($line =~ /^ +(read|write):.*IOPS=([\d\.]+)([a-z]*),.*BW=([\d\.]+)(MiB|KiB)/) {
		if ( $3 eq '' ) {
			$sequential_iops{$type} = $2;
		} elsif ( $3 eq 'k' ) {
			$sequential_iops{$type} = $2 * 1000;
		} else {
			print "#Error! unknown unit : $3";
		}

		if ( $5 eq 'MiB' ) {
			$sequential{$type} = $4;
		} else {
			$sequential{$type} = $4 / 1000;
		}

		if ($type eq 'write' ) {
			last;
		}
	}
}

while (my $line = <STDIN>) {
	if ($line =~ /^fio_b(\d+)k_r(\d+)_q(\d+)/) {
		($blocksize, $ratio, $iodepth) = ($1, $2, $3);
		$random{$blocksize}->{$iodepth}->{$ratio} = 0;
		$random_bw{$blocksize}->{$iodepth}->{$ratio} = 0;
	} elsif ($line =~ /^ +(read|write).*IOPS=([\d\.]+)([a-z]*),.*BW=([\d\.]+)(MiB|KiB)/) {
		if ( $3 eq '' ) {
			$random{$blocksize}->{$iodepth}->{$ratio} += $2;
		} elsif ( $3 eq 'k' ) {
			$random{$blocksize}->{$iodepth}->{$ratio} += $2 * 1000;
		} else {
			print "#Error! unknown unit : $3";
		}
	
		if ( $5 eq 'MiB' ) {
			$random_bw{$blocksize}->{$iodepth}->{$ratio} += $4;
		} else {
			$random_bw{$blocksize}->{$iodepth}->{$ratio} += $4 / 1000;
		}
	}
}

printf "[BW]\n";
printf "sequential read:\t%.1f\t[MiB/s]\n", $sequential{'read'};
printf "sequential write:\t%.1f\t[MiB/s]\n", $sequential{'write'};
print "random read/write\n";

foreach $blocksize (sort { $a <=> $b } keys %random_bw) {
	$header = 1;
	foreach $iodepth (sort { $a <=> $b } keys %{$random_bw{$blocksize}}) {
		if ($header) {
			print "BlockSize(KiB)\tQueueDepth\t";
			foreach $ratio (sort { $b <=> $a } keys %{$random_bw{$blocksize}->{$iodepth}}) {
				printf "r%d:w%d\t", $ratio, 100 - $ratio;
			}
			print "\n";
			$header = 0;
		}

		print "$blocksize\t$iodepth\t";
		foreach $ratio (sort { $b <=> $a } keys %{$random_bw{$blocksize}->{$iodepth}}) {
			print $random_bw{$blocksize}->{$iodepth}->{$ratio} . "\t";
		}
		print "\n";
	}
}
print "\n";

printf "[IOPS]\n";
printf "sequential read:\t%d\n", $sequential_iops{'read'};
printf "sequential write:\t%d\n", $sequential_iops{'write'};
print "random read/write\n";

foreach $blocksize (sort { $a <=> $b } keys %random) {
	$header = 1;
	foreach $iodepth (sort { $a <=> $b } keys %{$random{$blocksize}}) {
		if ($header) {
			print "BlockSize(KiB)\tQueueDepth\t";
			foreach $ratio (sort { $b <=> $a } keys %{$random{$blocksize}->{$iodepth}}) {
				printf "r%d:w%d\t", $ratio, 100 - $ratio;
			}
			print "\n";
			$header = 0;
		}

		print "$blocksize\t$iodepth\t";
		foreach $ratio (sort { $b <=> $a } keys %{$random{$blocksize}->{$iodepth}}) {
			print $random{$blocksize}->{$iodepth}->{$ratio} . "\t";
		}
		print "\n";
	}
}

