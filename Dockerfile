FROM centos:7.8.2003

MAINTAINER Kazuhiko Higashi

RUN yum -y install fio && \
    yum clean all && \
    rm -rf /var/cache/yum

ADD fio.sh /usr/local/bin/
ADD fio_summarize.pl /usr/local/bin/
ADD kick_fio.sh /usr/local/bin/

CMD /usr/local/bin/kick_fio.sh

